// import '../scss/main.scss';
// import '../fonts/WorkSans-Roman-VF.ttf';

// import '../vectors/10calc.svg';
// import '../vectors/campzy.svg';
import "../vectors/campzy-dark.svg";
// import '../vectors/befriend.svg';
// import '../vectors/button-github.svg';
// import '../vectors/button-linkedin.svg';
// import '../vectors/button-mail.svg';
// import '../vectors/button-telegram.svg';
// import '../vectors/github-logo.svg';
// import '../vectors/microsoft-logo.svg';
// import '../vectors/website-logo.svg';
// import '../vectors/sun.svg';
import "../vectors/moon.svg";

// // Images
// import '../images/10Calc.png';
// import '../images/smartmockups_jnfz7k1g.png';
// import '../images/befriend.png';
// import '../images/gicdungri.png';
// import '../images/Profile.jpeg';
// import '../images/macha.png';
// import '../images/rajbhasha.png';
// import '../images/10Calc.webp';
// import '../images/smartmockups_jnfz7k1g.webp';
// import '../images/befriend.webp';
// import '../images/gicdungri.webp';
// import '../images/Profile.webp';
// import '../images/macha.webp';
// import '../images/rajbhasha.webp';

// // Videos
// import '../videos/Camera-Potential-Doodle.mp4';

if (document.readyState !== "loading") {
  document.getElementById("loader").classList.add("hidden");
} else {
  document.addEventListener("DOMContentLoaded", () => {
    document.getElementById("loader").classList.add("hidden");
  });
}

const flkty = new Flickity(".main-carousel", {
  // options
  wrapAround: false,
  autoPlay: 5000,
  accessibility: true,
  pageDots: true,
  prevNextButtons: true,
  setGallerySize: false,
});

// Dark theme
const btn = document.querySelector(".btn-toggle");
const btnImage = document.querySelector(".btn-toggle-img");
const campzyLogo = document.querySelector("#campzy-logo");
const prefersDarkScheme = window.matchMedia("(prefers-color-scheme: dark)");

if (prefersDarkScheme.matches) {
  btnImage.src = "./vectors/moon.svg";
  campzyLogo.src = "./vectors/campzy-dark.svg";
} else {
  btnImage.src = "./vectors/sun.svg";
  campzyLogo.src = "./vectors/campzy.svg";
}

const currentTheme = localStorage.getItem("theme");
if (currentTheme == "dark") {
  document.body.classList.toggle("dark-theme");
  btnImage.src = "./vectors/moon.svg";
  campzyLogo.src = "./vectors/campzy-dark.svg";
} else if (currentTheme == "light") {
  document.body.classList.toggle("light-theme");
  btnImage.src = "./vectors/sun.svg";
  campzyLogo.src = "./vectors/campzy.svg";
}

btn.addEventListener("click", () => {
  if (btnImage.src === `${window.location.origin}/vectors/sun.svg`) {
    btnImage.src = "./vectors/moon.svg";
    campzyLogo.src = "./vectors/campzy-dark.svg";
  } else {
    btnImage.src = "./vectors/sun.svg";
    campzyLogo.src = "./vectors/campzy.svg";
  }
  if (prefersDarkScheme.matches) {
    document.body.classList.toggle("light-theme");
    var theme = document.body.classList.contains("light-theme")
      ? "light"
      : "dark";
  } else {
    document.body.classList.toggle("dark-theme");
    var theme = document.body.classList.contains("dark-theme")
      ? "dark"
      : "light";
  }
  localStorage.setItem("theme", theme);
});
